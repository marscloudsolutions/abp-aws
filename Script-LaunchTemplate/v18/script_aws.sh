#!/bin/bash
#Agafem els valors de les variables (tags o etiquetes)
export ip_empresa=$(curl 169.254.169.254/latest/meta-data/public-ipv4)

export INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)

export EMPRESA=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=EMPRESA" | jq .Tags[].Value | sed "s/\"//g")

export wordpress=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=wordpress" | jq .Tags[].Value | sed "s/\"//g")

export moodle=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=moodle" | jq .Tags[].Value | sed "s/\"//g")

#Crear el subdomini automaticament amb el nom de l'empresa
envsubst < /home/base/record.json.base > /home/docker/record.json
aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/record.json

#Crear la xarxa del docker
docker network create red_$EMPRESA

#Crear el docker del proxy amb el valor de les variables EMPRESA
envsubst < /home/base/proxy_invers/docker-compose.yml.base > /home/docker/proxy_invers/docker-compose.yml
docker-compose -f /home/docker/proxy_invers/docker-compose.yml up -d

#Si es vol el wordpress
if [ $wordpress == "true" ]
then
#Crear el sub-subdomini automaticament amb el nom de l'empresa
envsubst < /home/base/route53/wordpress/record.json.base > /home/docker/route53/wordpress/record.json
aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/wordpress/record.json

#Crear el docker del wordpress amb el valor de les variables EMPRESA
envsubst < /home/base/wordpress/docker-compose.yml.base_proxy > /home/docker/wordpress/docker-compose.yml
docker-compose -f /home/docker/wordpress/docker-compose.yml up -d
fi

if [ $moodle == "true" ]
then
#Crear el sub-subdomini automaticament amb el nom de l'empresa
envsubst < /home/base/route53/moodle/record.json.base > /home/docker/route53/moodle/record.json
aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/moodle/record.json

#Crear el docker del moodle amb el valor de les variables EMPRESA
envsubst < /home/base/moodle/docker-compose.yml.base_proxy > /home/docker/moodle/docker-compose.yml
docker-compose -f /home/docker/moodle/docker-compose.yml up -d
fi
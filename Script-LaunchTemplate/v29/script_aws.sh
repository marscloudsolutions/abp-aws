#!/bin/bash
#Agafem els valors de les variables (tags o etiquetes)
export ip_empresa=$(curl 169.254.169.254/latest/meta-data/public-ipv4)

export INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)

export EMPRESA=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=EMPRESA" | jq .Tags[].Value | sed "s/\"//g")

export wordpress=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=wordpress" | jq .Tags[].Value | sed "s/\"//g")

export moodle=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=moodle" | jq .Tags[].Value | sed "s/\"//g")

export nextcloud=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=nextcloud" | jq .Tags[].Value | sed "s/\"//g")

export joomla=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=joomla" | jq .Tags[].Value | sed "s/\"//g")

export jira=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=jira" | jq .Tags[].Value | sed "s/\"//g")

export mediawiki=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=mediawiki" | jq .Tags[].Value | sed "s/\"//g")

export ftp=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=ftp" | jq .Tags[].Value | sed "s/\"//g")

#Crear el subdomini automaticament amb el nom de l'empresa
envsubst < /home/base/record.json.base > /home/docker/record.json
aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/record.json

#Crear la xarxa del docker
docker network create red_$EMPRESA

#Crear el docker del proxy amb el valor de les variables EMPRESA
envsubst < /home/base/proxy_invers/docker-compose.yml.base > /home/docker/proxy_invers/docker-compose.yml
docker-compose -f /home/docker/proxy_invers/docker-compose.yml up -d

#Si es vol el wordpress i/o joomla i/o nextcloud i/o moodle i/o mediawiki i/o jira
if [ $wordpress == "true" ]
then
	#Crear el docker del wordpress amb el subdomini empresa.masrcloud.com
	#Crear el docker del wordpress amb el valor de les variables EMPRESA
	envsubst < /home/base/wordpress/docker-compose.yml.base_proxy > /home/docker/wordpress/docker-compose.yml
	docker-compose -f /home/docker/wordpress/docker-compose.yml up -d

	#Si es vol el moodle
	if [ $moodle == "true" ]
	then
		#Crear el sub-subdomini moodle.empresa.masrcloud.com
		envsubst < /home/base/route53/moodle/record.json.base > /home/docker/route53/moodle/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/moodle/record.json
		#Crear el docker del moodle apuntant a moodle.empresa.masrcloud.com
		#Crear el docker del moodle amb el valor de les variables EMPRESA
		envsubst < /home/base/moodle/docker-compose.yml.base > /home/docker/moodle/docker-compose.yml
		docker-compose -f /home/docker/moodle/docker-compose.yml up -d
	fi

	#Si es vol el nextcloud
	if [ $nextcloud == "true" ]
	then
		#Crear el sub-subdomini nextcloud.empresa.masrcloud.com
		envsubst < /home/base/route53/nextcloud/record.json.base > /home/docker/route53/nextcloud/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/nextcloud/record.json
		#Crear el docker del nextcloud apuntant a nextcloud.empresa.masrcloud.com
		#Crear el docker del nextcloud amb el valor de les variables EMPRESA
		envsubst < /home/base/nextcloud/docker-compose.yml.base > /home/docker/nextcloud/docker-compose.yml
		docker-compose -f /home/docker/nextcloud/docker-compose.yml up -d
	fi
	
	#Si es vol el joomla
	if [ $joomla == "true" ]
	then
		#Crear el sub-subdomini www.empresa.masrcloud.com
		envsubst < /home/base/route53/joomla/record.json.base > /home/docker/route53/joomla/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/joomla/record.json
		#Crear el docker del joomla apuntant a www.empresa.masrcloud.com
		#Crear el docker del joomla amb el valor de les variables EMPRESA
		envsubst < /home/base/joomla/docker-compose.yml.base > /home/docker/joomla/docker-compose.yml
		docker-compose -f /home/docker/joomla/docker-compose.yml up -d
	fi

	#Si es vol el jira
	if [ $jira == "true" ]
	then
		#Crear el sub-subdomini jira.empresa.masrcloud.com
		envsubst < /home/base/route53/jira/record.json.base > /home/docker/route53/jira/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/jira/record.json
		#Crear el docker del jira apuntant a jira.empresa.masrcloud.com
		#Crear el docker del jira amb el valor de les variables EMPRESA
		envsubst < /home/base/jira/docker-compose.yml.base > /home/docker/jira/docker-compose.yml
		docker-compose -f /home/docker/jira/docker-compose.yml up -d
	fi

	#Si es vol el mediawiki
	if [ $mediawiki == "true" ]
	then
		#Crear el sub-subdomini wiki.empresa.masrcloud.com
		envsubst < /home/base/route53/mediawiki/record.json.base > /home/docker/route53/mediawiki/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/mediawiki/record.json
		#Crear el docker del mediawiki apuntant a wiki.empresa.masrcloud.com
		#Crear el docker del mediawiki amb el valor de les variables EMPRESA
		envsubst < /home/base/mediawiki/docker-compose.yml.base > /home/docker/mediawiki/docker-compose.yml
		docker-compose -f /home/docker/mediawiki/docker-compose.yml up -d
	fi

#Si es vol el joomla i/o nextcloud i/o moodle i/o mediawiki i/o jira
elif [ $joomla == "true" ]
then
	#Crear el docker del joomla amb el subdomini empresa.masrcloud.com (s'ha de crear un nou compose amb el virtual_host modificat)
	envsubst < /home/base/joomla/docker-compose.yml.base_proxy > /home/docker/joomla/docker-compose.yml
	docker-compose -f /home/docker/joomla/docker-compose.yml up -d

	#Si es vol el nextcloud
	if [ $nextcloud == "true" ]
	then
		#Crear el sub-subdomini nextcloud.empresa.masrcloud.com
		envsubst < /home/base/route53/nextcloud/record.json.base > /home/docker/route53/nextcloud/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/nextcloud/record.json
		#Crear el docker del nextcloud apuntant a nextcloud.empresa.masrcloud.com
		#Crear el docker del nextcloud amb el valor de les variables EMPRESA
		envsubst < /home/base/nextcloud/docker-compose.yml.base > /home/docker/nextcloud/docker-compose.yml
		docker-compose -f /home/docker/nextcloud/docker-compose.yml up -d
	fi
	
	#Si es vol el moodle
	if [ $moodle == "true" ]
	then
		#Crear el sub-subdomini moodle.empresa.masrcloud.com
		envsubst < /home/base/route53/moodle/record.json.base > /home/docker/route53/moodle/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/moodle/record.json
		#Crear el docker del moodle apuntant a moodle.empresa.masrcloud.com
		#Crear el docker del moodle amb el valor de les variables EMPRESA
		envsubst < /home/base/moodle/docker-compose.yml.base > /home/docker/moodle/docker-compose.yml
		docker-compose -f /home/docker/moodle/docker-compose.yml up -d
	fi

	#Si es vol el jira
	if [ $jira == "true" ]
	then
		#Crear el sub-subdomini jira.empresa.masrcloud.com
		envsubst < /home/base/route53/jira/record.json.base > /home/docker/route53/jira/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/jira/record.json
		#Crear el docker del jira apuntant a jira.empresa.masrcloud.com
		#Crear el docker del jira amb el valor de les variables EMPRESA
		envsubst < /home/base/jira/docker-compose.yml.base > /home/docker/jira/docker-compose.yml
		docker-compose -f /home/docker/jira/docker-compose.yml up -d
	fi

	#Si es vol el mediawiki
	if [ $mediawiki == "true" ]
	then
		#Crear el sub-subdomini wiki.empresa.masrcloud.com
		envsubst < /home/base/route53/mediawiki/record.json.base > /home/docker/route53/mediawiki/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/mediawiki/record.json
		#Crear el docker del mediawiki apuntant a wiki.empresa.masrcloud.com
		#Crear el docker del mediawiki amb el valor de les variables EMPRESA
		envsubst < /home/base/mediawiki/docker-compose.yml.base > /home/docker/mediawiki/docker-compose.yml
		docker-compose -f /home/docker/mediawiki/docker-compose.yml up -d
	fi

#Si es vol el nextcloud i/o moodle i/o mediawiki i/o jira
elif [ $nextcloud == "true" ]
then
	#Crear el docker del nextcloud amb el subdomini empresa.masrcloud.com (s'ha de crear un nou compose amb el virtual_host modificat)
	envsubst < /home/base/nextcloud/docker-compose.yml.base_proxy > /home/docker/nextcloud/docker-compose.yml
	docker-compose -f /home/docker/nextcloud/docker-compose.yml up -d

	#Si es vol el moodle
	if [ $moodle == "true" ]
	then
		#Crear el sub-subdomini moodle.empresa.masrcloud.com
		envsubst < /home/base/route53/moodle/record.json.base > /home/docker/route53/moodle/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/moodle/record.json
		#Crear el docker del moodle apuntant a moodle.empresa.masrcloud.com
		#Crear el docker del moodle amb el valor de les variables EMPRESA
		envsubst < /home/base/moodle/docker-compose.yml.base > /home/docker/moodle/docker-compose.yml
		docker-compose -f /home/docker/moodle/docker-compose.yml up -d
	fi

	#Si es vol el jira
	if [ $jira == "true" ]
	then
		#Crear el sub-subdomini jira.empresa.masrcloud.com
		envsubst < /home/base/route53/jira/record.json.base > /home/docker/route53/jira/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/jira/record.json
		#Crear el docker del jira apuntant a jira.empresa.masrcloud.com
		#Crear el docker del jira amb el valor de les variables EMPRESA
		envsubst < /home/base/jira/docker-compose.yml.base > /home/docker/jira/docker-compose.yml
		docker-compose -f /home/docker/jira/docker-compose.yml up -d
	fi

	#Si es vol el mediawiki
	if [ $mediawiki == "true" ]
	then
		#Crear el sub-subdomini wiki.empresa.masrcloud.com
		envsubst < /home/base/route53/mediawiki/record.json.base > /home/docker/route53/mediawiki/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/mediawiki/record.json
		#Crear el docker del mediawiki apuntant a wiki.empresa.masrcloud.com
		#Crear el docker del mediawiki amb el valor de les variables EMPRESA
		envsubst < /home/base/mediawiki/docker-compose.yml.base > /home/docker/mediawiki/docker-compose.yml
		docker-compose -f /home/docker/mediawiki/docker-compose.yml up -d
	fi

#Si es vol el moodle i/o mediawiki i/o jira
elif [ $moodle == "true" ]
then	
	envsubst < /home/base/moodle/docker-compose.yml.base_proxy > /home/docker/moodle/docker-compose.yml
	docker-compose -f /home/docker/moodle/docker-compose.yml up -d
	
	#Si es vol el jira
	if [ $jira == "true" ]
	then
		#Crear el sub-subdomini jira.empresa.masrcloud.com
		envsubst < /home/base/route53/jira/record.json.base > /home/docker/route53/jira/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/jira/record.json
		#Crear el docker del jira apuntant a jira.empresa.masrcloud.com
		#Crear el docker del jira amb el valor de les variables EMPRESA
		envsubst < /home/base/jira/docker-compose.yml.base > /home/docker/jira/docker-compose.yml
		docker-compose -f /home/docker/jira/docker-compose.yml up -d
	fi

	#Si es vol el mediawiki
	if [ $mediawiki == "true" ]
	then
		#Crear el sub-subdomini wiki.empresa.masrcloud.com
		envsubst < /home/base/route53/mediawiki/record.json.base > /home/docker/route53/mediawiki/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/mediawiki/record.json
		#Crear el docker del mediawiki apuntant a wiki.empresa.masrcloud.com
		#Crear el docker del mediawiki amb el valor de les variables EMPRESA
		envsubst < /home/base/mediawiki/docker-compose.yml.base > /home/docker/mediawiki/docker-compose.yml
		docker-compose -f /home/docker/mediawiki/docker-compose.yml up -d
	fi

#Si es vol el mediawiki i/o jira
elif [ $mediawiki == "true" ]
then	
	envsubst < /home/base/mediawiki/docker-compose.yml.base_proxy > /home/docker/mediawiki/docker-compose.yml
	docker-compose -f /home/docker/mediawiki/docker-compose.yml up -d
	
	#Si es vol el jira
	if [ $jira == "true" ]
	then
		#Crear el sub-subdomini jira.empresa.masrcloud.com
		envsubst < /home/base/route53/jira/record.json.base > /home/docker/route53/jira/record.json
		aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/route53/jira/record.json
		#Crear el docker del jira apuntant a jira.empresa.masrcloud.com
		#Crear el docker del jira amb el valor de les variables EMPRESA
		envsubst < /home/base/jira/docker-compose.yml.base > /home/docker/jira/docker-compose.yml
		docker-compose -f /home/docker/jira/docker-compose.yml up -d
	fi

#Si es vol el jira
elif [ $jira == "true" ]
then	
	envsubst < /home/base/jira/docker-compose.yml.base_proxy > /home/docker/jira/docker-compose.yml
	docker-compose -f /home/docker/jira/docker-compose.yml up -d
fi

#Si es vol el ftp
if [ $ftp == "true" ]
then
	#Crear el docker del ftp amb el valor de les variables EMPRESA
	envsubst < /home/base/ftp/docker-compose.yml.base > /home/docker/ftp/docker-compose.yml
	docker-compose -f /home/docker/ftp/docker-compose.yml up -d
fi

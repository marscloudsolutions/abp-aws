#!/bin/bash

export ip_empresa=$(curl 169.254.169.254/latest/meta-data/public-ipv4)
export INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)
export EMPRESA=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=EMPRESA" | jq .Tags[].Value | sed "s/\"//g")

envsubst < /home/wordpress/docker-compose.yml.base > /home/ubuntu/docker-compose.yml
envsubst < /home/record.json.base > /home/ubuntu/record.json

aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/ubuntu/record.json

docker-compose -f /home/ubuntu/docker-compose.yml up -d
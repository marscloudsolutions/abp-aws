version: '3'

services:
  jira:
    depends_on:
      - postgresql
    image: teamatldocker/jira
    networks:
      - red_$EMPRESA
    volumes:
      - jiradata:/var/atlassian/jira
    environment:
      - 'JIRA_DB_PASSWORD=2021$EMPRESA'
      - 'SETENV_JVM_MINIMUM_MEMORY=2048m'
      - 'SETENV_JVM_MAXIMUM_MEMORY=4096m'
      - 'VIRTUAL_HOST=$EMPRESA.marscloudsolutions.com'
      - 'LETSENCRYPT_HOST=$EMPRESA.marscloudsolutions.com'
      - 'VIRTUAL_PORT=8080'
    logging:
      # limit logs retained on host to 25MB
      driver: "json-file"
      options:
        max-size: "500k"
        max-file: "50"

  postgresql:
    image: postgres:9.5-alpine
    networks:
      - red_$EMPRESA
    volumes:
      - postgresqldata:/var/lib/postgresql/data
    environment:
      - 'POSTGRES_USER=$EMPRESA'
      - 'POSTGRES_PASSWORD=2021$EMPRESA'
      - 'POSTGRES_DB=jiradb_$EMPRESA'
      - 'POSTGRES_ENCODING=UNICODE'
      - 'POSTGRES_COLLATE=C'
      - 'POSTGRES_COLLATE_TYPE=C'
    logging:
      # limit logs retained on host to 25MB
      driver: "json-file"
      options:
        max-size: "500k"
        max-file: "50"

volumes:
  jiradata:
    external: false
  postgresqldata:
    external: false

networks:
  red_$EMPRESA:
    external: true

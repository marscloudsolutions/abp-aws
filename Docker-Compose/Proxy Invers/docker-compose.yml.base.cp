version: '2'

services:
  nginx-proxy:
    container_name: proxy_invers
    image: jwilder/nginx-proxy
    restart: always
    ports:
      - "443:443"
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro
      - ./certs:/etc/nginx/certs
networks:
  default:
    external:
      name: red_$EMPRESA


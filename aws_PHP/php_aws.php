template AWS PHP

// Carreguem el vendor AWS
require 'aws/aws-autoloader.php';

// Configurem utilizar el modul del client EC2
use Aws\Ec2\Ec2Client;

/* Crear nou client que recollir les credenciales de .aws/credentials 
(utilitzar sempre regio us-east-1, és la nostra capa gratuita)*/
$client = new Aws\Ec2\Ec2Client([
    'region' => 'us-east-1',
    'version' => '2016-11-15',
    'profile' => 'default'
]);

// Ejecutar run-instances
$result = $client->runInstances([  
// Definir ID de launch template 
'LaunchTemplate' => [
    'LaunchTemplateId' => 'lt-00f47af9cb356b1e2'        
],
// Definir numero de instancias a aixecar
'MaxCount' => 1,
'MinCount' => 1,
// Definir tags que recolliran valors de variables setejades del formulari
'TagSpecifications' => [
    [
        'ResourceType' => 'instance',
        'Tags' => [
            [
                'Key' => 'EMPRESA',
                'Value' => $EMPRESA,
            ],
            [
                'Key' => 'Name',
                'Value' => $EMPRESA,
            ],
            [
                'Key' => 'wordpress',
                'Value' => $wordpress,
            ],
            [
                'Key' => 'moodle',
                'Value' => $moodle,
            ],
            [
                'Key' => 'nextcloud',
                'Value' => $nextcloud,
            ],
            [
                'Key' => 'ftp',
                'Value' => $ftp,
            ],
            [
                'Key' => 'jira',
                'Value' => $jira,
            ],
            [
                'Key' => 'mediawiki',
                'Value' => $mediawiki,
            ],            
   //       [
   //           'Key' => 'rocketchat',
   //           'Value' => $rocketchat,
   //       ],
            [
                'Key' => 'joomla',
                'Value' => $joomla,
            ],
           
        ],
    ],
],
]);
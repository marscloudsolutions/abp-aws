<?php
// Iniciem la sessió
session_start();
 
$_SESSION = array();
 
// destruim la sessió
session_destroy();
 
// Rdireccionem a la pagina principal
header("location: index.php");
exit;
?>
<?php
// Inicia la sessio
session_start();

// Mira si estas logueado, si estas te reedirige a la pagina principal
if (isset($_SESSION["logueado"]) && $_SESSION["logueado"] === true) {
    header("location: marscloudmain.php");
    exit;
}

// Incluimos el archivo de configuracion
require_once "marscloudconfig.php";

// Definimos variables i inicializamos valores vacios
$usuario = $password = "";
$usuario_err = $password_err = "";

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Check if usuario is empty
    if (empty(trim($_POST["usuario"]))) {
        $usuario_err = "Por favor ingrese su usuario.";
    } else {
        $usuario = trim($_POST["usuario"]);
    }

    // Check if password is empty
    if (empty(trim($_POST["password"]))) {
        $password_err = "Por favor ingrese su contraseña.";
    } else {
        $password = trim($_POST["password"]);
    }

    // Validate credentials
    if (empty($usuario_err) && empty($password_err)) {
        // Prepare a select statement
        $sql = "SELECT id, usuario, password FROM cuenta WHERE usuario = ?";

        if ($stmt = mysqli_prepare($link, $sql)) {
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_usuario);

            // Set parameters
            $param_usuario = $usuario;

            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)) {
                // Store result
                mysqli_stmt_store_result($stmt);

                // Check if usuario exists, if yes then verify password
                if (mysqli_stmt_num_rows($stmt) == 1) {
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $usuario, $hashed_password);
                    if (mysqli_stmt_fetch($stmt)) {
                        if (password_verify($password, $hashed_password)) {
                            // Password is correct, so start a new session
                            session_start();

                            // Store data in session variables
                            $_SESSION["logueado"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["usuario"] = $usuario;

                            // Redirect user to welcome page
                            header("location: marscloudmain.php");
                        } else {
                            // Display an error message if password is not valid
                            $password_err = "La contraseña que has ingresado no es válida.";
                        }
                    }
                } else {
                    // Display an error message if usuario doesn't exist
                    $usuario_err = "No existe cuenta registrada con ese nombre de usuario.";
                }
            } else {
                echo "Algo salió mal, por favor vuelve a intentarlo.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Close connection
    mysqli_close($link);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link href="styles/marscloud.css" rel="stylesheet">
    <style type="text/css">
        body {
            font: 14px sans-serif;
            text-align: center;
            width: 100vw;
            height: 100vh
        }

        .wrapper {
            width: 350px;
            padding: 20px;
        }
    </style>
</head>

<body class="d-flex flex-column justify-content-center align-items-center">
    <div class="wrapper">
        <h2><img src="images/logomars.png" width="200" height="200"></h2>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class=" <?php echo (!empty($usuario_err)) ? 'has-error' : ''; ?>">
                <input type="text" name="usuario" placeholder="Usuari" class="form-control" value="<?php echo $usuario; ?>">
                <span class="help-block"><?php echo $usuario_err; ?></span>
            </div>
            <div class=" <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <input type="password" placeholder="Contrasenya" name="password" class="form-control">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div>
                <input type="submit" class="btn btn-primary" value="Login">
            </div>
        </form>
        <div class="underlay-photo"></div>
        <div class="underlay-black"></div>
    </div>
</body>

</html>
<?php
// Inicia la sessió
session_start();

// Comprova que està loguejat
if (isset($_SESSION["logueado"]) && $_SESSION["logueado"] === true) {
    header("location: marscloudmain.php");
    exit;
}

// Incuim arxiu de  configuració BBDD
require_once "marscloudconfig.php";

// Definim variables de la BBDD
$usuario = $password = "";
$usuario_err = $password_err = "";

// Enviarem les dades amb POST
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Comprovem la variable usuari
    if (empty(trim($_POST["usuario"]))) {
        $usuario_err = "Si us plau, introdueix el teu usuari.";
    } else {
        $usuario = trim($_POST["usuario"]);
    }

    // Comprovem  la variable password
    if (empty(trim($_POST["password"]))) {
        $password_err = "Si us plau, introdueix la teva contrasenya.";
    } else {
        $password = trim($_POST["password"]);
    }

    // Validem credencials
    if (empty($usuario_err) && empty($password_err)) {
        // Preparem la query per consultar el usuari
        $sql = "SELECT id, usuario, password FROM cuenta WHERE usuario = ?";

        if ($stmt = mysqli_prepare($link, $sql)) {

            mysqli_stmt_bind_param($stmt, "s", $param_usuario);


            $param_usuario = $usuario;


            if (mysqli_stmt_execute($stmt)) {

                mysqli_stmt_store_result($stmt);

                //Comprovem si existeix l'usuari, si existeix comprovem el password
                if (mysqli_stmt_num_rows($stmt) == 1) {
                    mysqli_stmt_bind_result($stmt, $id, $usuario, $hashed_password);
                    if (mysqli_stmt_fetch($stmt)) {
                        if (password_verify($password, $hashed_password)) {
                            // Si el password és correcte, iniciem la sessió.
                            session_start();

                            // guardem les variables
                            $_SESSION["logueado"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["usuario"] = $usuario;

                            // redireccionem a la pagina principal
                            header("location: marscloudmain.php");
                        } else {
                            // mostrem error si la contrasenya no és valida
                            $password_err = "La contrasenya que has introduït no és vàlida.";
                        }
                    }
                } else {
                    // Mostrem error si l'usuari no existeix
                    $usuario_err = "No existeix cap compte registrat amb aquest nom d'usuari.";
                }
            } else {
                echo "Ups! Alguna cosa no ha funcionat, si us plau torna a provar.";
            }
        }


        mysqli_stmt_close($stmt);
    }

    // Tanquem la sessió a la BBDD
    mysqli_close($link);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link href="styles/marscloud.css" rel="stylesheet">
    <style type="text/css">
        body {
            font: 14px sans-serif;
            text-align: center;
            width: 100vw;
            height: 100vh
        }

        .wrapper {
            width: 350px;
            padding: 20px;
        }
    </style>
</head>

<body class="d-flex flex-column justify-content-center align-items-center">
    <div class="wrapper">
        <h2><img src="images/logomars.png" width="200" height="200"></h2>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class=" <?php echo (!empty($usuario_err)) ? 'has-error' : ''; ?>">
                <input type="text" name="usuario" placeholder="Usuari" class="form-control" value="<?php echo $usuario; ?>">
                <span class="help-block"><?php echo $usuario_err; ?></span>
            </div>
            <div class=" <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <input type="password" placeholder="Contrasenya" name="password" class="form-control">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div>
                <input type="submit" class="btn btn-primary" value="Login">
            </div>
        </form>
        <div class="underlay-photo"></div>
        <div class="underlay-black"></div>
    </div>
</body>

</html>
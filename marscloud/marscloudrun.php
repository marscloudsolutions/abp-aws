<?php
// Inicialitzem la sessió
session_start();

// Comprovem el login, si no redirigim a la pagina principal
if (!isset($_SESSION["logueado"]) || $_SESSION["logueado"] !== true) {
    header("location: index.php");
    exit;
}


// Setejar variables buides para evitar errors al fer-les en run-instances
$EMPRESA = '';
$wordpress = '';
$moodle = '';
$nextcloud = '';
$ftp = '';
$mediawiki = '';
$jira = '';
$joomla = '';
//$rocketchat = '';



// Recollir mom de l'empresa i les variables dels serveis que estan setejades, cambiem valor a "true"
if (isset($_POST["empresa"]) && !empty($_POST["empresa"])) {
    $EMPRESA = $_POST["empresa"];
}

if (isset($_POST["wordpress"]) && !empty($_POST["wordpress"])) {
    $wordpress = "true";
}
if (isset($_POST["moodle"]) && !empty($_POST["moodle"])) {
    $moodle = "true";
}
if (isset($_POST["nextcloud"]) && !empty($_POST["nextcloud"])) {
    $nextcloud = "true";
}
if (isset($_POST["ftp"]) && !empty($_POST["ftp"])) {
    $ftp = "true";
}
if (isset($_POST["mediawiki"]) && !empty($_POST["mediawiki"])) {
    $mediawiki = "true";
}
if (isset($_POST["jira"]) && !empty($_POST["jira"])) {
    $jira = "true";
}
if (isset($_POST["joomla"]) && !empty($_POST["joomla"])) {
    $joomla = "true";
}
/*if (isset($_POST["rocketchat"]) && !empty($_POST["rocketchat"])) { // penden d'implementar
    $rocketchat = "true";
}
*/
// Carreguem el vendor AWS
require 'aws/aws-autoloader.php';

// Configurem utilizar el modul del client EC2
use Aws\Ec2\Ec2Client;

/* Crear nou client que recollir les credenciales de .aws/credentials 
(utilitzar sempre regio us-east-1, és la nostra capa gratuita)*/
$client = new Aws\Ec2\Ec2Client([
    'region' => 'us-east-1',
    'version' => '2016-11-15',
    'profile' => 'default'
]);

// Ejecutar run-instances
$result = $client->runInstances([  
// Definir ID de launch template 
'LaunchTemplate' => [
    'LaunchTemplateId' => 'lt-00f47af9cb356b1e2'        
],
// Definir numero de instancias a aixecar
'MaxCount' => 1,
'MinCount' => 1,
// Definir tags que recolliran valors de variables setejades del formulari
'TagSpecifications' => [
    [
        'ResourceType' => 'instance',
        'Tags' => [
            [
                'Key' => 'EMPRESA',
                'Value' => $EMPRESA,
            ],
            [
                'Key' => 'Name',
                'Value' => $EMPRESA,
            ],
            [
                'Key' => 'wordpress',
                'Value' => $wordpress,
            ],
            [
                'Key' => 'moodle',
                'Value' => $moodle,
            ],
            [
                'Key' => 'nextcloud',
                'Value' => $nextcloud,
            ],
            [
                'Key' => 'ftp',
                'Value' => $ftp,
            ],
            [
                'Key' => 'jira',
                'Value' => $jira,
            ],
            [
                'Key' => 'mediawiki',
                'Value' => $mediawiki,
            ],            
   //       [
   //           'Key' => 'rocketchat',
   //           'Value' => $rocketchat,
   //       ],
            [
                'Key' => 'joomla',
                'Value' => $joomla,
            ],
           
        ],
    ],
],
]);

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link href="styles/marscloud.css" rel="stylesheet">
    <style type="text/css">
        body {
            font: 14px sans-serif;
            text-align: center;
            width: 100vw;
            height: 100vh
        }

        .wrapper {
            width: 350px;
            padding: 20px;
        }
        .url-servei {
            width: 350px;
            padding: 5px;
            color: white;
            background-color: rgba(255, 255, 255, 0.2);  
        }
    </style>
</head>

<body class="d-flex flex-column justify-content-center align-items-center">
    <div class="wrapper">
        <h2><img src="images/logomars.png" width="200" height="200"></h2>
        <div class="url-servei">


            <?php
            echo "<h3>El teu domini principal és </h3>";
            echo '<a href="https://' . $EMPRESA . '.marscloudsolutions.com"> <h3>' . $EMPRESA . '.marscloudsolutions.com</h3>';
            ?>

        </div>

        <div class="mt-4">
            <a href="marscloudmain.php" class="btn btn-primary">Crear una altre servei</a>
        </div>
        <div class="mt-4">
            <a href="marscloudlogout.php" class="btn btn-danger">Tanca la sessió</a>
        </div>

        <div class="underlay-photo"></div>
        <div class="underlay-black"></div>
    </div>
</body>

</html>
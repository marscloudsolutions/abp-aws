<?php
// Credencial per connectar ala BBDD
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'cuenta');
 
/* Connectem amb la BBDD */
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
 
// Comprovem l'enllaç
if($link === false){
    die("ERROR: Connectant a la BBDD. " . mysqli_connect_error());
}
?>